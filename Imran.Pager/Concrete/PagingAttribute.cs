﻿//Author Imran Ali
//Email : imran@guitarguru.in

namespace Imran.Pager.Concrete
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;

    [AttributeUsage(AttributeTargets.Class, Inherited = true)]
    public class PagingAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext actionContext)
        {
            try
            {
                var controller = actionContext.Controller as Imran.Pager.Mvc.Controller;
                if (actionContext.HttpContext.Request.QueryString["page"] != null)
                {
                    controller.Page = int.Parse(actionContext.HttpContext.Request.QueryString["page"].ToString());
                    if (controller.Page < 1)
                        controller.Page = 1;
                }
                else
                    controller.Page = 1;
                if (actionContext.HttpContext.Request.QueryString["size"] != null)
                    controller.Size = int.Parse(actionContext.HttpContext.Request.QueryString["size"].ToString());

                if (controller.Size < 1)
                {
                    controller.Size = 20;

                }
                controller.Skip = (controller.Page - 1) * (controller.Size);

                if (actionContext.HttpContext.Request.QueryString["q"] != null)
                {
                    controller.Query = actionContext.HttpContext.Request.QueryString["q"].ToString();
                    controller.search = true;
                }
                else
                {
                    controller.search = false;

                }
            }
            catch { 
            
            }

           
        }
    }
}
