﻿//Author Imran Ali
//Email : imran@guitarguru.in

namespace Imran.Pager.Mvc
{
    using Imran.Pager.Concrete;
    using System.Web.Mvc;

    public static class ViewResultExtension
    {
        public static ViewResult Page(this ViewResult Result, int Page, int Size, int Total, int Segment)
        {
            Result.ViewBag.PagerData = Pagers.Items(Total).PerPage(Size).Move(Page).Segment(Segment).Center();
            return Result;
        }
    }
}
