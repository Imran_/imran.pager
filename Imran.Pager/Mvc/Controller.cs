﻿//Author Imran Ali
//Email : imran@guitarguru.in

namespace Imran.Pager.Mvc
{

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Diagnostics.CodeAnalysis;
    using Imran.Pager.Concrete;
    using System.Web.Mvc;

    [Paging]
    public class Controller :System.Web.Mvc.Controller
    {
        public int Count;
        public int Page = 0;
        public int Size = 20;
        public int Skip = 0;
        public string Query = "";
        public bool search = false;


        protected internal AdaptiveResult AdaptiveView()
        {
            return AdaptiveView(null /* viewName */, null /* masterName */, null /* model */);
        }

        protected internal AdaptiveResult AdaptiveView(object model)
        {
            return AdaptiveView(null /* viewName */, null /* masterName */, model);
        }

        protected internal AdaptiveResult AdaptiveView(string viewName)
        {
            return AdaptiveView(viewName, null /* masterName */, null /* model */);
        }

        protected internal AdaptiveResult AdaptiveView(string viewName, string masterName)
        {
            return AdaptiveView(viewName, masterName, null /* model */);
        }

        protected internal AdaptiveResult AdaptiveView(string viewName, object model)
        {
            return AdaptiveView(viewName, null /* masterName */, model);
        }

        protected internal virtual AdaptiveResult AdaptiveView(string viewName, string masterName, object model)
        {
            if (model != null)
            {
                ViewData.Model = model;
            }

            return new AdaptiveResult
            {
                ViewName = viewName,
                MasterName = masterName,
                ViewData = ViewData,
                TempData = TempData
            };
        }

        [SuppressMessage("Microsoft.Naming", "CA1719:ParameterNamesShouldNotMatchMemberNames", MessageId = "0#", Justification = "The method name 'View' is a convenient shorthand for 'CreatAdaptiveViewResult'.")]
        protected internal AdaptiveResult AdaptiveView(IView view)
        {
            return AdaptiveView(view, null /* model */);
        }

        [SuppressMessage("Microsoft.Naming", "CA1719:ParameterNamesShouldNotMatchMemberNames", MessageId = "0#", Justification = "The method name 'View' is a convenient shorthand for 'CreatAdaptiveViewResult'.")]
        protected internal virtual AdaptiveResult AdaptiveView(IView view, object model)
        {
            if (model != null)
            {
                ViewData.Model = model;
            }

            return new AdaptiveResult
            {
                View = view,
                ViewData = ViewData,
                TempData = TempData
            };
        }

    }
}
