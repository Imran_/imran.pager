﻿//Author Imran Ali
//Email : imran@guitarguru.in 

namespace Imran.Pager.Mvc
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Imran.Pager.Concrete;

    public static class AdaptiveResultExtension
    {
        /// <summary>
        /// Results in paging
        /// </summary>
        /// <param name="Result">The current instance of EduResult</param>
        /// <param name="Page">Page index</param>
        /// <param name="Size">No of records to show</param>
        /// <param name="Total">Total records</param>
        /// <param name="Segment">No of Paging Segment</param>
        /// <returns>Current Instance of Eduresult</returns>
        public static AdaptiveResult Page(this AdaptiveResult Result, int Page, int Size, int Total, int Segment)
        {
            Result.ViewBag.PagerData =Pagers.Items(Total).PerPage(Size).Move(Page).Segment(Segment).Center();
            return Result;
        }
    }
}
