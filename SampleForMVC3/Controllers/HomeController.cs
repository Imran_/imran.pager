﻿

namespace SampleForMVC3.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using FakeDataProvider;
    using Imran.Pager.Mvc;

    public class HomeController : Imran.Pager.Mvc.Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {

            var repo = new FakeRepo();
            return View(repo.Persons.OrderBy(i => i.Birthday).Skip(Skip).Take(Size)).Page(Page, Size, repo.Persons.Count, 5);
        }
        public ActionResult About()
        {

            return View();
        }
    }
}
