﻿//Author Imran Ali
//Email : imran@guitarguru.in

namespace Imran.Pager.Concrete
{
    using System;
    using System.Globalization;
    using System.Text;
    using System.Web.Mvc;

    
    public class AdaptiveResult : ViewResult
    {
        private string _masterName;

        public string MasterName
        {
            get
            {
                return _masterName ?? String.Empty;
            }
            set
            {
                _masterName = value;
            }
        }

        protected override ViewEngineResult FindView(ControllerContext context)
        {
            if (context.HttpContext.Request.IsAjaxRequest() || context.IsChildAction)
            {
                context.Controller.ViewBag.AdatpiveView = true;
                ViewEngineResult result = ViewEngineCollection.FindPartialView(context, ViewName);
                if (result.View != null)
                {
                    return result;
                }

                // we need to generate an exception containing all the locations we searched
                StringBuilder locationsText = new StringBuilder();

                foreach (string location in result.SearchedLocations)
                {
                    locationsText.AppendLine();
                    locationsText.Append(location);
                }
                throw new InvalidOperationException(String.Format(CultureInfo.CurrentCulture,
                     ViewName, locationsText));
            }
            else
            {
                ViewEngineResult result = ViewEngineCollection.FindView(context, ViewName, MasterName);
                if (result.View != null)
                {
                    return result;
                }

                // we need to generate an exception containing all the locations we searched
                StringBuilder locationsText = new StringBuilder();

                foreach (string location in result.SearchedLocations)
                {
                    locationsText.AppendLine();
                    locationsText.Append(location);
                }
                throw new InvalidOperationException(String.Format(CultureInfo.CurrentCulture,
                     ViewName, locationsText));
            }
        }
    }
}
