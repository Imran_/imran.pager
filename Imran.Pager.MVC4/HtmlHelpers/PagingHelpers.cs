﻿//Author Imran Ali
//Email : imran@guitarguru.in


namespace Imran.Pager.HtmlHelpers
{
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using System.Web.Mvc.Html;
    using System.Web.Routing;
    using Imran.Pager.Concrete;
   
    public static class PagingHelpers
    {
        public static MvcHtmlString Pager(this HtmlHelper Html)
        {
            if (Html.ViewContext.Controller.ViewBag.PagerData == null)
                return new MvcHtmlString("");

            var con = Html.ViewContext.Controller as Imran.Pager.Mvc.Controller;
            Pagers p = Html.ViewContext.Controller.ViewBag.PagerData;
            var List = Html.ViewContext.RequestContext.RouteData.Values;
            if (List == null)
                List = new RouteValueDictionary();
            var quer = Html.ViewContext.Controller.ControllerContext.RequestContext.HttpContext.Request.QueryString;
            if (quer.Count > 0)
            {
                foreach (var i in quer.Cast<string>())
                {
                    if (List.ContainsKey(i))
                    {
                        List.Remove(i);
                    }
                    List.Add(i, quer[i]);
                }
            }
            //Html.ViewContext.RequestContext.RouteData.Values=List;
            var div = new TagBuilder("div");
            div.AddCssClass("paging_bootstrap pagination");
            var ul = new TagBuilder("ul");
            StringBuilder sb = new StringBuilder();
            if (p.HasPreviousPage)
            {
                sb.Append(Html.lia(p.FirstPageIndex, "first", "First").ToString());
                sb.Append(Html.lia(p.PreviousPageIndex, "prev ", "← Previous").ToString());
            }
            foreach (var i in p)
            {
                if (i == con.Page)
                    sb.Append(Html.lia(i, "active").ToString());
                else
                    sb.Append(Html.lia(i).ToString());
            }
            if (p.HasNextPage)
            {
                sb.Append(Html.lia(p.NextPageIndex, "next ", "Next →").ToString());
                sb.Append(Html.lia(p.LastPageIndex, "last", "Last").ToString());
            }
            ul.InnerHtml = sb.ToString();
            div.InnerHtml = ul.ToString();
            return new MvcHtmlString(div.ToString());


        }

        public static MvcHtmlString lia(this HtmlHelper Html, int l, string clas = "", string ls = "", bool active = false)
        {
            var controller = Html.ViewContext.RequestContext.RouteData.Values["Controller"].ToString();
            var Action = Html.ViewContext.RequestContext.RouteData.Values["Action"].ToString();
            var List = Html.ViewContext.RequestContext.RouteData.Values;
            if (List == null)
                List = new RouteValueDictionary();
            if (List.ContainsKey("page"))
                List.Remove("page");
            List.Add("page", l);


            var li = new TagBuilder("li");
            if (ls == "")
                li.InnerHtml = Html.ActionLink(l.ToString(), Action, List).ToString();
            else
                li.InnerHtml = Html.ActionLink(ls, Action, List).ToString();
            if (clas != "")
                li.AddCssClass(clas);
            return new MvcHtmlString(li.ToString());
        }
    }
}
