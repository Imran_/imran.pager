﻿//Author Imran Ali
//Email : imran@guitarguru.in

namespace FakeDataProvider
{
    using System;

    public class Person
    {
        public int Id { get; set; }
        public string First { get; set; }
        public string Last { get; set; }
        public DateTime Birthday { get; set; }

        
    }
}
