﻿//Author Imran Ali
//Email : imran@guitarguru.in

namespace FakeDataProvider
{
    using System.Collections.Generic;
    using FizzWare.NBuilder;

    public class FakeRepo
    {
        public List<Person> Persons { get; set; }
        public FakeRepo()
        {
            var person = Builder<Person>.CreateListOfSize(200)
                               .All()
                               .Build();
            Persons = new List<Person>();
            Persons.AddRange(person);
        }

    }
}
